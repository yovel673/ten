#pragma once
#include <string>

class Item
{
public:
	Item(std::string name, std::string serialNumber, double unitPrice);
	~Item();


	double totalPrice() const; 
	bool operator <(const Item& other) const; 
	bool operator >(const Item& other) const; 
	bool operator ==(const Item& other) const;
	std::string getName() const;
	std::string getSerial() const;
	int getCount() const;
	int getPrice() const;
	void incCount();
	void decCount();

private:

	std::string _name;
	std::string _serialNumber;
	int _count;
	double _unitPrice;

};
