#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string name);
	Customer();
	double totalSum() const;
	void addItem(Item toAdd);
	void removeItem(Item toRemove);
	void PrintItems();
	std::string getName();
private:
	std::string _name;
	std::set<Item> _items;
};
