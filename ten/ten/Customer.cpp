#include "Customer.h"
#include "Item.h"
#include <iostream>
#include <set>

Customer::Customer(std::string name)
{
	this->_name = name;
}

Customer::Customer()
{
	//nothing to delete
}


double Customer::totalSum() const
{
	double counter = 0;

	for (std::set<Item>::iterator i = _items.begin(); i != _items.end(); i++)
	{
		counter += i->totalPrice();
	}
	return counter;
}




void Customer::addItem(Item toAdd)
{
	std::set<Item>::iterator it = _items.find(toAdd);
	if (it == _items.end())
	{
		_items.insert(toAdd);
	}
	else
	{
		Item temp = *it;
		temp.incCount();
		_items.erase(it);
		_items.insert(temp);
	}

}

void Customer::removeItem(Item toRemove)
{
	std::set<Item>::iterator it = _items.find(toRemove);
	if (it == _items.end())
	{
		std::cout << "error, item not found";
	}
	else
	{
		if (it->getCount() == 1)
		{
			_items.erase(it);
		}
		else
		{
			Item temp = *it;
			temp.decCount();
			_items.erase(it);
			_items.insert(temp);
		}
	}
}

void Customer::PrintItems()
{
	std::cout << _name << "'s items" << std::endl;
	for (std::set<Item>::iterator it = _items.begin(); it != _items.end(); it++)
	{
		std::cout << it->getCount() << " times " << it->getName() << ". price =" << it->getPrice() << std::endl;
	}


}

std::string Customer::getName()
{
	return std::string();
}
