#include "Item.h"
#include <string>

Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	_name = name;
	_serialNumber = serialNumber;
	_unitPrice = unitPrice;
	_count = 1;

}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return ((this->_count) * (this->_unitPrice));
}

bool Item::operator<(const Item & other) const
{
	if(_serialNumber < other.getSerial())
		return true;
	return false;
}


bool Item::operator>(const Item & other) const
{
	if (_serialNumber > other.getSerial())
		return true;
	return false;

}

bool Item::operator==(const Item & other) const
{
	if (_serialNumber == other.getSerial())
		return true;
	return false;

}

std::string Item::getName() const
{
	return this->_name;
}

std::string Item::getSerial() const
{
	return this->_serialNumber;
}

int Item::getCount() const
{
	return this->_unitPrice;
}

int Item::getPrice() const
{
	return this->_unitPrice;
}

void Item::incCount()
{
	this->_count++;
}

void Item::decCount()
{
	this->_count--;
}
